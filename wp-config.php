<?php
if ( file_exists( dirname( __FILE__ ) . '/wp-config-local.php' ) ) {
	include dirname( __FILE__ ) . '/wp-config-local.php';
} else {
/**
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'db_web_laboratory');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'X3L9VcBeGD6FuLvH');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'Nc1  a,$sGC>#{ZHloE]!6W}}>51+R,+8%^]hg{EpfyCZ##GWVD6>_%.Q lNt>07');
define('SECURE_AUTH_KEY', 'S7bhA7]6d}=UV-(d/tja.9F+DX<$|!N;L<*GPRu/znDpYvx;l(hC)*%=}<}:4x=Q');
define('LOGGED_IN_KEY', '-+p]?RSx gOs}+iDv;w+.R*+qVj{qQjVz}H_@f1pK2HTVudo$6_Xl`^,94.*xjRr');
define('NONCE_KEY', '=3@$ 04Dr3vtq9s;]79A6mWta`[WQ^IN2a>t@js-)=)^v]hW7QV*;.WD}pQIpW,i');
define('AUTH_SALT', 'i p:1h<Jyv.;e9ypJQK$,Go<^,SnmUq&-v1`Lr+4qtAUV};r#9-hW+>hc}.tA#ie');
define('SECURE_AUTH_SALT', '2<D s&. C?09VMX[k:%N}g<QTZpQidDq|1Y+bn#y3*5Cl63)=AuxR<Q-@flS4&i}');
define('LOGGED_IN_SALT', ' .#7An^{TwP{z$rX?0$Qd`O^74uF$v$wUf1?^~;Rq((5sAF>n(ySXIY?DeHM;($f');
define('NONCE_SALT', ' )NNp3[&.RO/;Vvw(un%JfhfV^3]+Qj8[!<|1V:t~&4~A{j<COJ|-c+n7Trq!}8C');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wl_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);
define('WP_DEBUG', false);
define('FS_METHOD', 'direct');

define('WP_HOME',    'http://web-laboratory.com');
define('WP_SITEURL', 'http://web-laboratory.com');
define('DISABLE_WP_CRON', true);


/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
}
